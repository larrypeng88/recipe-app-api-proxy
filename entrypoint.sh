#!/bin/sh
# Stop the script is there is any error.
set -e

# We pass in the .tpl file in the Docker to the envsust cmd
# Then envsubst passes in the environment variables to the vars in {} in .tpl
# Then output a new file to the conf.d location where NGINX read its config
envsubst <  /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# By default NGINX runs a daemon background service
# Start NGINX service in the foreground for Docker. All the logs output from server get printed at the Docker output
nginx -g 'daemon off;'
